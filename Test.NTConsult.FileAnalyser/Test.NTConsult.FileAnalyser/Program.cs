﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Test.NTConsult.FileAnalyser.DomainModels;

namespace Test.NTConsult.FileAnalyser
{
    class Program
    {
        private static readonly IConfigurationRoot _configuration;
        private static readonly string _sourcePath;
        private static readonly string _destinationPath;

        static Program()
        {
            WriteLog($"Welcome to the NTConsult test!\n");

            _configuration = BuildConfiguration();
            _sourcePath = _configuration.GetSection("infrastructure:sourceFolder").Value;
            _destinationPath = _configuration.GetSection("infrastructure:destinationFolder").Value;
            CreateDirectories();
        }

        static void Main(string[] args)
        {
            try
            {
                var watcher = new FileSystemWatcher
                {
                    Path = _configuration.GetSection("infrastructure:sourceFolder").Value,
                    EnableRaisingEvents = true,
                    Filter = "*.txt"
                };

                watcher.Created += new FileSystemEventHandler(ReadAndProcessFileAfterCreated);

                WriteLog("Listening...");
                WriteLog("(Press any key to exit.)");

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                WriteLog("An error has ocurred:\n");
                WriteLog($"{ex.Message}\n");
            }
        }

        private static void ReadAndProcessFileAfterCreated(object sender, FileSystemEventArgs e)
        {
            if (!e.FullPath.EndsWith(".txt"))
            {
                WriteLog($"File '{e.Name}' has and invalid extension.");
            }

            var lines = new List<string>();

            using (var reader = File.OpenText(e.FullPath))
            {
                string line = string.Empty;
                
                while ((line = reader.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            if (!lines.Any())
                WriteLog($"No data was found on file '{e.Name}'.");

            var report = BuildReportBasedOnFileContentLines(lines);

            WriteReportResultsInFile(report);
            RemoveSourceFile(e.Name);
            
            WriteLog($"File '{e.Name}' was read and processed successfully.");
        }

        private static Report BuildReportBasedOnFileContentLines(IList<string> lines)
        {
            var report = new Report();

            foreach (var line in lines)
            {
                var contentSplit = line.Split('ç');

                if (contentSplit[0] == "001")
                {
                    var salesman = new Salesman(contentSplit[1], contentSplit[2], decimal.Parse(contentSplit[3]));
                    report.Salesmen.Add(salesman);
                    continue;
                }

                if (contentSplit[0] == "002")
                {
                    var client = new Client(contentSplit[1], contentSplit[2], contentSplit[3]);
                    report.Clients.Add(client);
                    continue;
                }

                if (contentSplit[0] == "003")
                {
                    var sale = new Sale(int.Parse(contentSplit[1]), contentSplit[3]);
                    
                    var saleItemsRaw = contentSplit[2][1..^1];
                    var saleItemsAsStrings = saleItemsRaw.Split(',');

                    if (!saleItemsAsStrings.Any())
                        continue;

                    foreach (var saleItemAsString in saleItemsAsStrings)
                    {
                        var saleItemSplit = saleItemAsString.Split('-');
                        var saleItem = new SaleItem(int.Parse(saleItemSplit[0]), int.Parse(saleItemSplit[1]), decimal.Parse(saleItemSplit[2]));
                        sale.SaleItems.Add(saleItem);
                    }

                    report.Sales.Add(sale);
                }
            }

            return report;
        }

        private static void WriteReportResultsInFile(Report report)
        {
            using (var streamWriter = new StreamWriter($"{_destinationPath}\\{Guid.NewGuid()}.txt"))
            {
                streamWriter.WriteLine($"Quantidade de Clientes: {report.AmountOfClients}");
                streamWriter.WriteLine($"Quantidade de Vendedores: {report.AmountOfSalesman}");
                streamWriter.WriteLine($"Id da Venda mais cara: {report.MostExpensiveSaleId}");
                streamWriter.WriteLine($"Pior Vendedor: {report.WorstSalesman}");

                streamWriter.Close();
            }

            WriteLog("Report was successfully writen in file.");
        }

        private static void WriteLog(string message)
        {
            Console.WriteLine(message);
        }
        
        private static void CreateDirectories()
        {
            if (!Directory.Exists(_sourcePath))
            {
                Directory.CreateDirectory(_sourcePath);
                WriteLog("Source path created successfully");
            }

            if (!Directory.Exists(_destinationPath))
            {
                Directory.CreateDirectory(_destinationPath);
                WriteLog("Destination path created successfully");
            }
        }

        private static void RemoveSourceFile(string filename)
        {
            File.Delete($"{_sourcePath}\\{filename}");

            WriteLog($"File '{filename}' was successfully deleted.");
        }

        private static IConfigurationRoot BuildConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            return builder.Build();
        }
    }
}
