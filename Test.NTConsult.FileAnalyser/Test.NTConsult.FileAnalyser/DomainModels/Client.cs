﻿namespace Test.NTConsult.FileAnalyser.DomainModels
{
    public class Client
    {
        public Client(string documentNumber, string name, string businessArea)
        {
            DocumentNumber = documentNumber;
            Name = name;
            BusinessArea = businessArea;
        }

        public string Name { get; }
        public string DocumentNumber { get; }
        public string BusinessArea { get; }
    }
}
