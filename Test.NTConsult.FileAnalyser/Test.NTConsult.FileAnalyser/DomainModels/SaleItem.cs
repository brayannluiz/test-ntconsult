﻿namespace Test.NTConsult.FileAnalyser.DomainModels
{

    public class SaleItem
    {
        public SaleItem(int id, int quantity, decimal price)
        {
            Id = id;
            Quantity = quantity;
            Price = price;
        }

        public int Id { get; }
        public int Quantity { get; }
        private decimal Price { get; }
        public decimal PriceTotal => Price * Quantity;
    }
}
