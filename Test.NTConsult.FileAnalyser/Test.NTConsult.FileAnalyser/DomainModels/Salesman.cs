﻿namespace Test.NTConsult.FileAnalyser.DomainModels
{
    public class Salesman
    {
        public Salesman(string documentNumber, string name, decimal salary)
        {
            DocumentNumber = documentNumber;
            Name = name;            
            Salary = salary;
        }

        public string Name { get; }
        public string DocumentNumber { get; }   
        public decimal Salary { get; }
    }
}
