﻿using System.Collections.Generic;
using System.Linq;

namespace Test.NTConsult.FileAnalyser.DomainModels
{
    public class Sale
    {
        public Sale(int id, string salesmanName)
        {
            Id = id;
            SalesmanName = salesmanName;
            SaleItems = new List<SaleItem>();
        }

        public int Id { get; }
        public string SalesmanName { get; }
        public decimal SaleValue => SaleItems.Any() ? SaleItems.Sum(s => s.PriceTotal) : 0;
            
        public IList<SaleItem> SaleItems { get; }
    }
}
