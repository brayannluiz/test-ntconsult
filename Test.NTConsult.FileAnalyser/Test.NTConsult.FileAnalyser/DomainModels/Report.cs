﻿using System.Collections.Generic;
using System.Linq;

namespace Test.NTConsult.FileAnalyser.DomainModels
{
    public class Report
    {
        public Report()
        {
            Clients = new List<Client>();
            Salesmen = new List<Salesman>();
            Sales = new List<Sale>();
        }

        public IList<Client> Clients { get; }
        public IList<Salesman> Salesmen { get; }
        public IList<Sale> Sales { get; }

        public int AmountOfClients => Clients.Count;
        public int AmountOfSalesman => Salesmen.Count;
        public int MostExpensiveSaleId => Sales.Any() ? Sales.OrderByDescending(s => s.SaleValue).First().Id : 0;
        public string WorstSalesman
        { 
            get
            {
                if (!Sales.Any())
                    return "Unavailable";

                var worstSalesman = from Sale in Sales
                                   group Sale by Sales.Sum(s => s.SaleValue) into GroupedSales
                                   orderby GroupedSales.Key
                                   select GroupedSales.Last().SalesmanName;

                return worstSalesman.First();
            }
        }
    }
}
